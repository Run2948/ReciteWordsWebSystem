package com.borun.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.borun.entity.UserBean;

public interface UserRepository extends JpaRepository<UserBean,Integer>{
	
	public UserBean findDistinctByUsername(String username);
	
	public UserBean findByUsernameAndPassword(String username,String password);
	
    @Query("from com.borun.entity.UserBean ub where 1=1 and ub.username like %:username%")
    public Page<UserBean> findByCondition(@Param("username") String username, Pageable pageable);
}
