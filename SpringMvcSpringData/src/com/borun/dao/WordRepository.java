package com.borun.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.borun.entity.WordBaseBean;
import com.borun.entity.WordBean;

public interface WordRepository extends JpaRepository<WordBean,Integer>{

	public WordBean findDistinctByEnglish(String english);
	
    @Query("from com.borun.entity.WordBean wb where 1=1 "
    		+ "and wb.chinese like %:chinese% "
    		+ "and wb.english like %:english%")
    public Page<WordBean> findByCondition(
    		@Param("chinese") String chinese,
    		@Param("english") String english,
            Pageable pageable);
    
    public Page<WordBean> findWordBeansByWordBaseBean(WordBaseBean wordBaseBean, Pageable pageable);
    
    public List<WordBean> findWordBeansByWordBaseBean(WordBaseBean wordBaseBean);
}
