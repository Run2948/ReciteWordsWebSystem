package com.borun.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.borun.entity.WordBaseBean;

public interface WordBaseRepository extends JpaRepository<WordBaseBean,Integer>{
	
	public WordBaseBean findDistinctByBasename(String basename);
	
    @Query("from com.borun.entity.WordBaseBean wb where 1=1 and wb.basename like %:basename%")
    public Page<WordBaseBean> findByCondition(@Param("basename") String basename,
                                     Pageable pageable);
}
