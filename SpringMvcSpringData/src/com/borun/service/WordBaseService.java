package com.borun.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.borun.entity.WordBaseBean;
import com.borun.entity.condition.WordBaseCondition;

public interface WordBaseService {
	
	public Page<WordBaseBean> getAll(int pageIndex, int pageSize);
	
	public List<WordBaseBean> getAll();
	
    public Page<WordBaseBean> getByCondition(WordBaseCondition wordBaseCondition);
    
    public WordBaseBean getDetail(Integer id);
        
    public WordBaseBean findByBaseName(String basename);
    
    public WordBaseBean getWordBase(WordBaseBean wordBaseBean);  
    
    public WordBaseBean saveOrupdate(WordBaseBean wordBaseBean);
    
    public void delete(Integer id);
    
    public void delete(List<Integer> ids);
	
}
