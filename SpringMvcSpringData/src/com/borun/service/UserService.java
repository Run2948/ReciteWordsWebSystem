package com.borun.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.borun.entity.UserBean;
import com.borun.entity.condition.UserCondition;


public interface UserService {
	
	public Page<UserBean> getAll(int pageIndex, int pageSize);
	
    public Page<UserBean> getByCondition(UserCondition userCondition);
    
    public UserBean getDetail(Integer id);
        
    public UserBean findByUsername(String username);
    
    public UserBean login(UserBean userBean);
    
    public UserBean regist(UserBean userBean);
    
    public UserBean saveOrupdate(UserBean userBean);
    
    public void delete(Integer id);
    
    public void delete(List<Integer> ids);
}
