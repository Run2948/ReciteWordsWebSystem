package com.borun.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.borun.entity.WordBaseBean;
import com.borun.entity.WordBean;
import com.borun.entity.condition.WordCondition;

public interface WordService {
	
	public Page<WordBean> getAll(int pageIndex, int pageSize);
	
    public Page<WordBean> getByCondition(WordCondition wordCondition);
    
    public WordBean getDetail(Integer id);
        
    public WordBean findByEnglish(String english);
    
    public WordBean getWord(WordBean wordBean);  
    
    public WordBean saveOrupdate(WordBean wordBean);
    
    public void delete(Integer id);
    
    public void delete(List<Integer> ids);
    
    Page<WordBean> findByWordBase(WordBaseBean wordBaseBean, Pageable pageable);

    Page<WordBean> findByWordBase(WordBaseBean wordBaseBean, int pageIndex, int pageSize);

    Page<WordBean> findByWordBase(Integer wordBaseId, int pageIndex, int pageSize);

    Page<WordBean> findByWordBase(Integer wordBaseId, Pageable pageable);
	
    List<WordBean> findByWordBase(Integer wordBaseId);
}
