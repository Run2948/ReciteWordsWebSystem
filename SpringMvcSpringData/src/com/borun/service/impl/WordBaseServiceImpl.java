package com.borun.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.borun.dao.WordBaseRepository;
import com.borun.entity.WordBaseBean;
import com.borun.entity.condition.WordBaseCondition;
import com.borun.service.WordBaseService;

@Service
public class WordBaseServiceImpl implements WordBaseService{

	@Autowired
    private WordBaseRepository wordBaseRepository;
	
	@Override
	public Page<WordBaseBean> getAll(int pageIndex, int pageSize) {
		return wordBaseRepository.findAll(new PageRequest(pageIndex - 1, pageSize));
	}

	@Override
	public Page<WordBaseBean> getByCondition(WordBaseCondition wordBaseCondition) {
		return wordBaseRepository.findByCondition(wordBaseCondition.getBasename(),new PageRequest(wordBaseCondition.getPageIndex(), wordBaseCondition.getPageSize()));
	}

	@Override
	public WordBaseBean getDetail(Integer id) {
		return wordBaseRepository.findOne(id);
	}

	@Override
	public WordBaseBean findByBaseName(String basename) {
		return wordBaseRepository.findDistinctByBasename(basename);
	}

	@Override
	public WordBaseBean getWordBase(WordBaseBean wordBaseBean) {
		if(wordBaseBean.getId() != 0)
			return wordBaseRepository.findOne(wordBaseBean.getId());
		else if(wordBaseBean.getBasename() != null)
			return wordBaseRepository.findDistinctByBasename(wordBaseBean.getBasename());
		else 
			return null;
	}

	@Override
	public WordBaseBean saveOrupdate(WordBaseBean wordBaseBean) {
		return wordBaseRepository.saveAndFlush(wordBaseBean);
	}

	@Override
	public void delete(Integer id) {
		if(id != null && id > 0)
			wordBaseRepository.delete(id);
	}

	@Override
	public void delete(List<Integer> ids) {
		if(ids != null && ids.size() > 0)
			for (Integer id:ids)
				delete(id);
	}

	@Override
	public List<WordBaseBean> getAll() {
		return wordBaseRepository.findAll();
	}

}
