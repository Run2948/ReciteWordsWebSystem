package com.borun.entity;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Cacheable
@Entity
@Table(name="t_word_base")
public class WordBaseBean {
	
	private Integer id;
	private String basename;

	public WordBaseBean(){
		
	}
	public WordBaseBean(Integer id){
		//this.id = id;
		this.setId(id);
	}
	
	@GeneratedValue
    @Id
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBasename() {
		return basename;
	}
	public void setBasename(String basename) {
		this.basename = basename;
	}
}