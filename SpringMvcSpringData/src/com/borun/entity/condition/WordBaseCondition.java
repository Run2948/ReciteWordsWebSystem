package com.borun.entity.condition;

public class WordBaseCondition extends BaseCondition{

	private String basename = "";

	public String getBasename() {
		return basename;
	}

	public void setBasename(String basename) {
		this.basename = basename;
	}
	
	
}
