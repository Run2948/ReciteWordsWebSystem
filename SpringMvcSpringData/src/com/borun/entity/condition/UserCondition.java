package com.borun.entity.condition;


public class UserCondition extends BaseCondition {

	private String username = "";

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
