package com.borun.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="t_word")
public class WordBean {
	
	private Integer id;
	private String english;
	private String chinese;	
	private WordBaseBean wordBaseBean;
	
	@GeneratedValue
    @Id	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEnglish() {
		return english;
	}
	public void setEnglish(String english) {
		this.english = english;
	}
	public String getChinese() {
		return chinese;
	}
	public void setChinese(String chinese) {
		this.chinese = chinese;
	}

    @JoinColumn(name="base_id")
    @ManyToOne(fetch=FetchType.EAGER)
	public WordBaseBean getWordBaseBean() {
		return wordBaseBean;
	}
	public void setWordBaseBean(WordBaseBean wordBaseBean) {
		this.wordBaseBean = wordBaseBean;
	}

	
	
}
