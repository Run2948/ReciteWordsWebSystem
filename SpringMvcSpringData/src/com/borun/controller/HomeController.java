package com.borun.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/Home")
public class HomeController {

	@RequestMapping(value = "/Index", method = RequestMethod.GET)
	public String Index(HttpServletRequest request) {
		
		return "/Index";
	}
}
