package com.borun.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/Account")
public class AccountController {

	/**
	 * 登录视图
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/Login", method = RequestMethod.GET)
	public String Login(HttpServletRequest request)  throws Exception {
		// TODO 判断有无session，有直接到首页
		if (request.getSession().getAttribute("userToken") != null) {
			return "redirect:/home/index";
		}
		return "/Login";
	}

	/**
	 * 注册视图
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/Regist", method = RequestMethod.GET)
	public String Regist(HttpServletRequest request)  throws Exception {
		return "/Register";
	}
	
		
	/**
	 * 退出系统
	 * 
	 * @param session
	 *            Session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/Logout")
	public String logout(HttpSession session) throws Exception {
		// 清除Session
		session.invalidate();
		return "redirect:Login";
	}

}
