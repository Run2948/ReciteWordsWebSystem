package com.borun.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.borun.entity.WordBean;
import com.borun.service.WordService;

@Controller
@RequestMapping("/Word")
public class WordController {

	@Autowired
	private WordService wordService;
	
	@RequestMapping(value = "/Index", method = RequestMethod.GET)
	public String Index(HttpServletRequest request) {
		
		return "word/Word";
	}
	
	@RequestMapping(value = "/Add", method = RequestMethod.GET)
	public String Add(HttpServletRequest request) {
		
		return "word/Add";
	}
	
	@RequestMapping(value = "/Edit/{id}", method = RequestMethod.GET)
	public String Update(HttpServletRequest request,@PathVariable Integer id) {
		WordBean wordBean = wordService.getDetail(id);
		request.setAttribute("editWord", wordBean);
		return "word/Edit";
	}
	
}
