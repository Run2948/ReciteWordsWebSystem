package com.borun.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.borun.entity.UserBean;
import com.borun.entity.condition.UserCondition;
import com.borun.service.UserService;
import com.borun.util.RespUtils;
import com.borun.util.entity.RespStatus;

@Controller
@ResponseBody
@RequestMapping("api/user/")
public class UserApiController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/getUsers",method=RequestMethod.GET)
	public JSONObject getAllUsers(UserCondition userCondition){
		Page<UserBean> userBeans = userService.getByCondition(userCondition);
		System.out.println("username:"+userCondition.getUsername());	
        return RespUtils.createSucResp(userBeans);
	}
	
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public JSONObject addUser(UserBean userBean,String confrim){
		if(!confrim.equals(userBean.getPassword()))
			return RespUtils.createResp(RespStatus.FALSEAUTH, "两次密码不一致");
		
		System.out.println("username:"+userBean.getUsername());	
		System.out.println("password:"+userBean.getPassword());			
		userService.saveOrupdate(userBean);
        return RespUtils.createSucResp("添加成功！");
	}
	
	
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public JSONObject editUser(UserBean userBean){
		System.out.println("userId:"+userBean.getId());	
		System.out.println("username:"+userBean.getUsername());	
		
		UserBean updateUser = userService.getDetail(userBean.getId());
		updateUser.setUsername(userBean.getUsername());
		
		userService.saveOrupdate(updateUser);
        return RespUtils.createSucResp("编辑成功！");
	}
	
	@RequestMapping(value="/set",method=RequestMethod.POST)
	public JSONObject setPwd(UserBean userBean,String newpass,String confrim){
		
		System.out.println("id:"+userBean.getId());	
		System.out.println("username:"+userBean.getUsername());	
		System.out.println("password:"+userBean.getPassword());
		
		if(!confrim.equals(newpass))
			return RespUtils.createResp(RespStatus.FALSEAUTH, "两次密码不一致");
			
		UserBean pwdUser = userService.getDetail(userBean.getId());
		
		if(!pwdUser.getUsername().equals(userBean.getUsername()))
			return RespUtils.createResp(RespStatus.FALSEAUTH, "非法用户！");
		
		if(!pwdUser.getPassword().equals(userBean.getPassword()))
			return RespUtils.createResp(RespStatus.FALSEAUTH, "原密码错误！");
				
		pwdUser.setPassword(newpass);		
		userService.saveOrupdate(pwdUser);
        return RespUtils.createSucResp("密码修改成功，新的密码将在下次登录时生效！");
	}
	
	
	@RequestMapping(value="/reset",method=RequestMethod.POST)
	public JSONObject resetUser(UserBean userBean){
		System.out.println("id:"+userBean.getId());	
		System.out.println("password:"+userBean.getPassword());	
		
		UserBean resetUser = userService.getDetail(userBean.getId());
		resetUser.setPassword(userBean.getPassword() == null ?"1234546":userBean.getPassword());		
		userService.saveOrupdate(resetUser);
		
        return RespUtils.createSucResp("重置成功！该用户密码："+ resetUser.getPassword());
	}
	
	
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	public JSONObject delUser(@RequestParam Integer id){
        try {
        	userService.delete(id);
        	System.out.println("id:"+id);
            return RespUtils.createSucResp("删除成功！");
        } catch (Exception e) {
            return RespUtils.createResp(RespStatus.DAOFAIL, "删除失败！");
        }
	}
	
	@RequestMapping(value="/deleteAll",method=RequestMethod.POST)
    public JSONObject delAllUser(@RequestParam String ids) {
        try {
            List<Integer> longs;
            System.out.println("ids:"+ids);
            longs = JSON.parseArray(ids, Integer.class);
            System.out.println("longs:"+longs);
            userService.delete(longs);
            return RespUtils.createSucResp("删除成功！");
        } catch (Exception e) {
        	System.out.println(e.toString());
            return RespUtils.createResp(RespStatus.DAOFAIL, "删除失败！");
        }
    }
	
	
}
