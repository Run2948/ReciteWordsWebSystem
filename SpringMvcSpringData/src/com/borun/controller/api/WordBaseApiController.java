package com.borun.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.borun.entity.WordBaseBean;
import com.borun.entity.condition.WordBaseCondition;
import com.borun.service.WordBaseService;
import com.borun.util.RespUtils;
import com.borun.util.entity.RespStatus;

@Controller
@ResponseBody
@RequestMapping("api/base/")
public class WordBaseApiController {

	@Autowired
	private WordBaseService wordBaseService;
	
	@RequestMapping(value="/getBases",method=RequestMethod.GET)
	public JSONObject getAllWordBases(WordBaseCondition wordBaseCondition){
		Page<WordBaseBean> baseBeans = wordBaseService.getByCondition(wordBaseCondition);
		System.out.println("baseName:"+wordBaseCondition.getBasename());	
        return RespUtils.createSucResp(baseBeans);
	}
	
	@RequestMapping(value="/getSelectBases",method=RequestMethod.GET)
	public JSONObject getSelectWordBases(){
		List<WordBaseBean> baseBeans = wordBaseService.getAll();
        return RespUtils.createSucResp(baseBeans);
	}
	
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public JSONObject addBase(WordBaseBean wordBaseBean){
		wordBaseService.saveOrupdate(wordBaseBean);
		System.out.println("baseName:"+wordBaseBean.getBasename());	
        return RespUtils.createSucResp("添加成功！");
	}
	
	
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public JSONObject editBase(WordBaseBean wordBaseBean){
		System.out.println("baseId:"+wordBaseBean.getId());	
		System.out.println("baseName:"+wordBaseBean.getBasename());	
		wordBaseService.saveOrupdate(wordBaseBean);
        return RespUtils.createSucResp("编辑成功！");
	}
	
	
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	public JSONObject delBase(@RequestParam Integer id){
        try {
        	wordBaseService.delete(id);
        	System.out.println("id:"+id);
            return RespUtils.createSucResp("删除成功！");
        } catch (Exception e) {
            return RespUtils.createResp(RespStatus.DAOFAIL, "删除失败！请先确保词库为空！");
        }
	}
	
	@RequestMapping(value="/deleteAll",method=RequestMethod.POST)
    public JSONObject delAllBase(@RequestParam String ids) {
        try {
            List<Integer> longs;
            System.out.println("ids:"+ids);
            longs = JSON.parseArray(ids, Integer.class);
            System.out.println("longs:"+longs);
            wordBaseService.delete(longs);
            return RespUtils.createSucResp("删除成功！");
        } catch (Exception e) {
        	System.out.println(e.toString());
            return RespUtils.createResp(RespStatus.DAOFAIL, "删除失败！");
        }
    }
}
