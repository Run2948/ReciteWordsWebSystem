package com.borun.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.borun.entity.WordBaseBean;
import com.borun.entity.WordBean;
import com.borun.entity.condition.WordCondition;
import com.borun.service.WordService;
import com.borun.util.RespUtils;
import com.borun.util.entity.RespStatus;

@Controller
@ResponseBody
@RequestMapping("api/word/")
public class WordApiController {

	@Autowired
	private WordService wordService;
	
	@RequestMapping(value="/getWords",method=RequestMethod.GET)
	public JSONObject getAllWords(WordCondition wordCondition){
		Page<WordBean> baseBeans = wordService.getByCondition(wordCondition);
		System.out.println("chinese:"+wordCondition.getChinese());	
		System.out.println("english:"+wordCondition.getEnglish());	
        return RespUtils.createSucResp(baseBeans);
	}
	
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public JSONObject addWord(WordBean wordBean,Integer base_id){
		System.out.println("baseid:"+base_id);	
		System.out.println("chinese:"+wordBean.getChinese());	
		System.out.println("english:"+wordBean.getEnglish());	
		wordBean.setWordBaseBean(new WordBaseBean(base_id));
		wordService.saveOrupdate(wordBean);
        return RespUtils.createSucResp("添加成功！");
	}
	
	
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	public JSONObject editWord(WordBean wordBean,Integer base_id){
		System.out.println("id:"+wordBean.getId());	
		System.out.println("chinese:"+wordBean.getChinese());	
		System.out.println("english:"+wordBean.getEnglish());
		System.out.println("base_id:"+base_id);
		wordBean.setWordBaseBean(new WordBaseBean(base_id));
		wordService.saveOrupdate(wordBean);
        return RespUtils.createSucResp("编辑成功！");
	}
	
	
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	public JSONObject delWord(@RequestParam Integer id){
        try {
        	wordService.delete(id);
        	System.out.println("id:"+id);
            return RespUtils.createSucResp("删除成功！");
        } catch (Exception e) {
            return RespUtils.createResp(RespStatus.DAOFAIL, "删除失败！");
        }
	}
	
	@RequestMapping(value="/deleteAll",method=RequestMethod.POST)
    public JSONObject delAllWord(@RequestParam String ids) {
        try {
            List<Integer> longs;
            System.out.println("ids:"+ids);
            longs = JSON.parseArray(ids, Integer.class);
            System.out.println("longs:"+longs);
            wordService.delete(longs);
            return RespUtils.createSucResp("删除成功！");
        } catch (Exception e) {
        	System.out.println(e.toString());
            return RespUtils.createResp(RespStatus.DAOFAIL, "删除失败！");
        }
    }
}
