package com.borun.util.entity;

public enum RespStatus {
	
    SUCCESS(100, "success"),
    //验证错误
    FALSEAUTH(200, "illegal auth"),
    //帐号错误
    ILLEAGACCOUNT(210, "illegal account"),
    //密码错误
    ILLEAGPASSWORD(220, "illegal password"),
    //未登录
    UNLOGIN(300, "unlogin"),
    //未注册
    UNREGIST(400, "unregist"),
    //帐号已经存在
    ACCOUNTEXITS(410, "account exits"),
    //数据库操作失败
    DAOFAIL(500, "dao fail"),
    //出现未知错误
    UNKNOWNERROR(600, "unknowerror"),
    //解析出错
    PARSEERROR(700, "parse error"),
    //短信发送失败
    SMSSENDERROR(800, "sms send error");

    private int code;
    private String desc;

    RespStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}