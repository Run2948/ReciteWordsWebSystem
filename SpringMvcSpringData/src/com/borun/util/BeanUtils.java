package com.borun.util;

import java.lang.reflect.Field;

public class BeanUtils {
	  @SuppressWarnings("rawtypes")
	public static <T> T meger(T persistent, T gift) {
			Class c = persistent.getClass();
	        Field[] declaredFields = gift.getClass().getDeclaredFields();
	        for (Field declaredField : declaredFields) {
	            try {
	                declaredField.setAccessible(true);
	                Object o = declaredField.get(gift);
	                if (o == null) continue;
	                Field persistentField = c.getDeclaredField(declaredField.getName());
	                persistentField.setAccessible(true);
	                if (!o.equals(persistentField.get(persistent)))
	                    persistentField.set(persistent, o);
	                declaredField.setAccessible(false);
	                persistentField.setAccessible(false);
	            } catch (IllegalAccessException | NoSuchFieldException e) {
	                throw new RuntimeException("字段解析异常:" + e.getMessage());
	            }
	        }
	        return persistent;
	    }
}
