package com.borun.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	private static final SimpleDateFormat dateFormatExact = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	private TimeUtils() {
	}

	public static Long parse(String time) {
		if (time == null)
			return 0L;
		try {
			return time.length() > 10 ? dateFormatExact.parse(time).getTime()
					: dateFormat.parse(time).getTime();
		} catch (ParseException e) {
			return 0L;
		}
	}

	public static String parse(Long time) {
		return dateFormatExact.format(new Date(time));
	}
}
