package com.borun.util;

import java.util.Random;

public class RandomUtils {

	//生成在[min,max]之间的随机整数，
	public static Integer generate(Integer min,Integer max){
		Random random = new Random();
		return random.nextInt(max)%(max-min+1) + min;
	}
	
}
