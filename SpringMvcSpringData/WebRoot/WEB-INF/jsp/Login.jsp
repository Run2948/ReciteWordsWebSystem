<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title>登录</title>
	<jsp:include page="common/partial/css.jsp"></jsp:include>
	<style type="text/css">
        body{
            background:#eee;
            padding-top:80px;
        }
    </style>
  </head>
  <body>
<div class="container">
        <div class="row clearfix">
            <div class="col-md-4 column">
            </div>
            <div class="col-md-4 column">
                <h1>欢迎登陆</h1>
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputUserName" placeholder="请输入您的登录账号" value="admin" required autofocus/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="inputPassword" placeholder="请输入您的登录密码" value="123456" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input">
                               	 记住我
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9">
                            <button type="button" class="btn btn-primary btn-block">登录</button>
                        </div>
                    </div>
                </form>
                <p>还没有账号？点击<a href="Account/Regist">快速注册</a></p>
            </div>
            <div class="col-md-4 column">           	
            </div>
        </div>
    </div>
    <jsp:include page="common/partial/js.jsp"></jsp:include>
    <script type="text/javascript">
    	$(".btn").click(function(){
    		var username = $("#inputUserName").val();
    		var password = $("#inputPassword").val();
    		
    		if(username == ''){
    			layer.msg("用户名不能为空！", { icon: 0, shift: 6, time: 1000 });
    			return;
    		}
    		
    		if(password == ''){
    			layer.msg("登录密码不能为空！", { icon: 0, shift: 6, time: 1000 });
    			return;
    		}
    		
    		$.ajax({
    			type:'POST',
    			url:'api/login',
    			data:{username:username,password:password},
    			dataType:'json',
    			success:function(result){
    				if(result != null && result.code > 100){   					
    					layer.msg(result.msg, { icon:0, shift: 6, time: 1000 });
    				}else{
    					layer.msg(result.msg+",正在前往首页...", { icon:6, shift: 4, time: 1000 });
    					setTimeout(function(){
    						window.location.href= "Home/Index";
    					},1000);
    				}
    			}
    		})  		
    	});
    </script>
  </body>
</html>
