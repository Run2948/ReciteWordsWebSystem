<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">  
    <title>帮助</title>
    <jsp:include page="../common/partial/css.jsp"></jsp:include>
  </head>  
  <body>
     <div class="container col-md-12">
		<jsp:include page="../common/partial/top.jsp"></jsp:include>
        <div class="row clearfix">
            <div class="col-md-12 column"> 
               <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">系统简介</h3>
                    </div>
                    <div class="panel-body">
                       	 <h4>我爱记单词   v1.0.0</h4>
                       	 <p>开发技术：Spring + SpringMvc + SpringData(Jpa)</p>
                       	 <p>JDK版本：jdk 1.7及以上</p>
                       	 <p>测试平台：Windows 10 家庭版</p>
                       	 <p>测试服务器：Tomcat 7</p>
                    </div>
                </div> 
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">帮助提示</h3>
                    </div>
                    <div class="panel-body">
                       	<p>该系统的设计意图是为我们寝室以及周围寝室的小伙伴们搭建一个四六级考试记单词的平台。该系统的主要使用流程如下：</p>
                       	<p>&nbsp;&nbsp;&nbsp;&nbsp;一、注册账户，建立自己的词库和单词；</p>
                       	<p>&nbsp;&nbsp;&nbsp;&nbsp;二、选择词库，随机单词进行背诵；</p>
                       	<p>&nbsp;&nbsp;&nbsp;&nbsp;三、根据得分，评估自己的单词积累；</p>
                    </div>
                </div>                   
               <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">关于作者</h3>
                    </div>
                    <div class="panel-body">
                       	 <h4>余丝</h4>
                       	 <p>性别：女&nbsp;&nbsp;&nbsp;&nbsp;籍贯：湖北孝感</p>
                       	 <p>学院：计算机科学与技术学院&nbsp;&nbsp;&nbsp;&nbsp;班级：16计本1班&nbsp;&nbsp;&nbsp;&nbsp;学号：163821045</p>
                    </div>
                </div>            
            </div>
        </div>
    </div>
    <jsp:include page="../common/partial/js.jsp"></jsp:include>
  </body>
</html>
