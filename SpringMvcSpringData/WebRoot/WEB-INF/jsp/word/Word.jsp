<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String wordPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=wordPath%>">  
    <title>词库管理</title>
    <jsp:include page="../common/partial/css.jsp"></jsp:include>
  </head>  
  <body>
     <div class="container col-md-12" id="wordDiv">
		<jsp:include page="../common/partial/top.jsp"></jsp:include>
        <div class="row clearfix">
            <div class="col-md-12 column">   
				<div class="row clearfix">
                    <div class="col-md-2 column">
                        <div class="list-group">
                            <a href="Word/Index" class="list-group-item active">
                                <span class="glyphicon glyphicon-list"></span> 单词列表
                            </a>
                            <a href="Word/Add" class="list-group-item">
                                <span class="glyphicon glyphicon-plus"></span> 新建单词
                            </a>
                        </div>
                    </div>
                    <div class="col-md-10 column">
                        <div>
	                        <ol class="breadcrumb">
	                            <li><a href="Home/Index">首页</a></li>
	                            <li><a href="Word/Index">单词管理</a></li>
	                            <li class="active">单词列表</li>
	                        </ol>
                        	<a href="Word/Add" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> 新建单词</a> 
							<span style="padding-left: 50px;">搜索：</span> 
							<span> 
								<input type="text" placeholder="请输入英文单词搜索" v-model="searchEnglish" class="form-control" style="width:250px; line-height:17px;display:inline-block" />
								<input type="text" placeholder="请输入汉语释义搜索" v-model="searchChinese" class="form-control" style="width:250px; line-height:17px;display:inline-block" />
								<button class="btn btn-default" v-on:click="search">查询</button>
							</span>
                        </div>
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>序号</th>
                                    <th>英文单词</th>
                                    <th>汉语释义</th>
                                    <th>所属词库</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(word,wordIndex) in words">
                                    <th scope="row"><input type="checkbox" name="id[]" v-bind:value="word.id"/>&nbsp;{{ wordIndex+1 }}</th>
                                    <td v-text="word.english"></td>
                                    <td v-text="word.chinese"></td>
                                    <td v-text="word.wordBaseBean.basename" class="label label-info" style="display: inline-block;"></td>
									<td>
										<div role="presentation" class="dropdown">
											<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">操作 <span class="caret"></span> </button>
											<ul class="dropdown-menu">
												<li><a v-bind:href="['Word/Edit/'+word.id]">编辑</a></li>
												<li><a href="javascript:void(0)" v-on:click="del(word.id)">删除</a></li>
											</ul>
										</div>
									</td>
                                </tr>
					            <tr>
				                    <th scope="row" colspan="5" v-show="this.count > 1">
				                        <input type="checkbox" id="checkall" value="" />&nbsp;&nbsp;<label for="checkall">全选</label>&nbsp;&nbsp;&nbsp;&nbsp;
				                        <a href="javascript:void(0)" class="btn btn-default" style="padding:5px 15px;" onclick="DelSelect()">批量删除</a>
				                    </th>
					            </tr>                                
                            </tbody>
                        </table>
						<!-- 由于数据太多，所以必须以分页的形式存在 -->
						<nav aria-label="Page navigation" v-if="totalPage > 1">
							<ul class="pagination">
								<li v-bind:class="{ disabled: currentPage == 1}" v-on:click="changePage(currentPage - 1)"><a href="javascript:void(0)" aria-label="Previous"> <span aria-hidden="true">上一页</span></a></li>
								<li v-if="totalPage > 10 && currentPage >= 6" v-on:click="changePage(1)"><a href="javascript:void(0)">1</a></li>
								<li v-if="totalPage > 10 && currentPage >= 6" class="disabled"> <a href="javascript:void(0)">...</a></li>
								<li v-if="(totalPage <= 10 ) || (currentPage <= 5 && p <= 8) || (currentPage >= totalPage - 5 && p > totalPage - 8) || (currentPage > 5 && currentPage <= totalPage - 5 && p > currentPage - 3 && p < currentPage + 4)" v-for="p in totalPage" v-bind:class="{active: p == currentPage}" v-on:click="changePage(p)"><a href="javascript:void(0)" v-text="p"></a></li>
								<li v-if="totalPage > 10 && currentPage < totalPage - 5" class="disabled"><a href="javascript:void(0)">...</a></li>
								<li v-if="totalPage > 10 && currentPage < totalPage - 5" v-on:click="changePage(totalPage)"><a href="javascript:void(0)" v-text="totalPage"></a></li>
								<li v-bind:class="{disabled: currentPage == totalPage}" v-on:click="changePage(currentPage + 1)"><a href="javascript:void(0)" aria-label="Next"> <span aria-hidden="true">下一页</span></a></li>
							</ul>
						</nav>
                    </div>
                </div>         
            </div>
        </div>
    </div>
    <jsp:include page="../common/partial/js.jsp"></jsp:include>
	<script type="text/javascript">
		//  请求数据的主体方法
		var wordApp = new Vue(
			{
				el : '#wordDiv',
				data : {
					words : [],
					count : null,
					currentPage : 1,
					totalPage : null,
					searchEnglish : "",
					searchChinese : ""
				},
				mounted : function() {

					// 穿插一个 全选方法
					$("#checkall").click(function() {
						if (this.checked) {
							$("input[name='id[]']").each(function() {
								this.checked = true;
							})
						} else {
							$("input[name='id[]']").each(function() {
								this.checked = false;
							})
						}
					});

					// 请求数据
					var temp = this;
					$.ajax({
						url : 'api/word/getWords?pageSize=8&pageIndex=0',
						type : "GET",
						dataType : 'json',
						timeout : 20 * 1000,
						contentType : 'application/json;charset=UTF-8',
						success : function(json) {
							if (json.code == 100) {
								temp.words = json.data.content;
								temp.currentPage = 1;
								temp.count = json.data.totalElements;
							}
						},
						error : function(XMLHttpRequest, textStatus, errorThrown) {
							alert("网络请求错误")
						}
					})
				},
				watch : {
					"count" : function(val, oldVal) {
						this.totalPage = parseInt(val / 8) + (val % 8 > 0 ? 1 : 0);
					}
				},
				methods : {
					search : function() {
						var temp = this;
						$.ajax(
								{
									url : "api/word/getWords?pageSize=8&pageIndex=0&english="+ temp.searchEnglish+"&chinese="+temp.searchChinese,
									type : "GET",
									dataType : 'json',
									timeout : 20 * 1000,
									contentType : 'application/json;charset=UTF-8',
									success : function(json) {
										if (json.code == 100 && json.data.totalElements > 0) {
											temp.words = json.data.content;
											temp.currentPage = 1;
											temp.count = json.data.totalElements;
										} else {
											alert("你搜索的内容不存在啊")
										}
									},
									error : function(XMLHttpRequest, textStatus, errorThrown) {
										//console.log("网络请求错误")
										alert("网络错误")
									},
								})
					},
					changePage : function(p) {
						if (p <= 0) {
							this.currentPage = 1;
						} else if (p > this.totalPage) {
							this.currentPage = this.totalPage;
						} else {
							this.currentPage = p;
						}
						this.submit(this.currentPage);
					},
					submit : function(page) {
						if (!page) {
							page = 1;
						}
						var temp = this;
						$.ajax({
							url : 'api/word/getWords?pageSize=8&pageIndex='+ (page - 1),
							type : 'GET',
							dateType : 'json',
							timeout : 20 * 1000,
							contentType : 'application/json;charset=UTF-8',
							success : function(json) {
								if (json.code == 100) {
									temp.words = json.data.content;
									temp.currentPage = page;
								}
							}
						})
					}
				}
			})
		
        // 单个删除
        function del(id) {
            //询问框
            layer.confirm('您确定要删除吗?', {
                shade: false,
                btn: ['确定', '取消'] //按钮
            }, function () {
                $.ajax({
                    type: 'POST',
                    url: 'api/word/delete',
                    data: { id: id },
                    dataType: 'json',
                    success: function (result) {
                        if (result != null && result.code > 100) {
                            layer.msg(result.msg, { icon: 0, shift: 6, time: 1000 })
                        } else {
                            layer.msg(result.msg, { icon: 6, shift: 4, time: 1000 });
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                });
            }, function () {

            });
        }

        //批量删除
        function DelSelect() {
            var becheckbox = '[';
            $("input[name='id[]']").each(function () { //遍历table里的全部checkbox
                if (this.checked == true) //如果被选中
                    becheckbox += $(this).val() + ","; //获取被选中的值
            });
            if (becheckbox.length > 1) { //如果获取到
                becheckbox = becheckbox.substring(0, becheckbox.length - 1); //把最后一个逗号去掉
                becheckbox += ']';
                //询问框
                layer.confirm('您确定要删除所有选中项吗?', {
                    shade: false,
                    btn: ['确定', '取消'] //按钮
                }, function () {
                    $.ajax({
                        type: 'POST',
                        url: 'api/word/deleteAll',
                        data: { ids: becheckbox },
                        dataType: 'json',
                        success: function (result) {
                            if (result != null && result.code > 100) {
                                layer.msg(result.msg, { icon: 0, shift: 6, time: 1000 })
                            } else {
                                layer.msg(result.msg, { icon: 6, shift: 4, time: 1000 });
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                            }
                        }
                    });
                }, function () {

                });
            } else {
                layer.msg("请至少选中一项！", { icon: 0, shift: 6, time: 1000 })
            }
        }
	</script>    
  </body>
</html>
