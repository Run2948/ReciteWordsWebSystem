<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String wordPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=wordPath%>">  
    <title>词库管理</title>
    <jsp:include page="../common/partial/css.jsp"></jsp:include>
  </head>  
  <body>
     <div class="container col-md-12">
		<jsp:include page="../common/partial/top.jsp"></jsp:include>
        <div class="row clearfix">
            <div class="col-md-12 column">   
				<div class="row clearfix">
                    <div class="col-md-2 column">
                        <div class="list-group">
                            <a href="Word/Index" class="list-group-item">
                                <span class="glyphicon glyphicon-list"></span> 单词列表
                            </a>
                            <a href="Word/Add" class="list-group-item active">
                                <span class="glyphicon glyphicon-plus"></span> 新建单词
                            </a>
                        </div>
                    </div>
                    <div class="col-md-10 column">
                        <div>
	                        <ol class="breadcrumb">
	                            <li><a href="Home/Index">首页</a></li>
	                            <li><a href="Word/Index">单词管理</a></li>
	                            <li class="active">新建单词</li>
	                        </ol>                        	
                        </div>
                       	<form class="col-md-5 column">
                       	     <div class="form-group">
				                <label for="title">英文单词</label>
				                <input type="text" id="English" name="english" class="form-control" placeholder="请输入英文单词">
				            </div>
                       	     <div class="form-group">
				                <label for="title">汉语释义</label>
				                <input type="text" id="Chinese" name="chinese" class="form-control" placeholder="请输入汉语释义">
				            </div>
				            <div class="form-group">
				                <label for="word_base">分配词库</label>
				                <select class="form-control" style="width:200px;" id="word_base" name="wordBase">
				                    <!-- <option value=""></option> -->
				                </select>
				            </div>
				            <div class="form-group">
				                <label for="important">
				                    
				                </label>
				                <button type="button" class="btn btn-primary pull-right" id="btnSubmit">保存单词</button>
				            </div>
                       	</form>
                    </div>
                </div>         
            </div>
        </div>
    </div>
    <jsp:include page="../common/partial/js.jsp"></jsp:include>
	<script type="text/javascript">
    // 获取下拉列表
    function fillData() {
        $.ajax({
            url: "api/base/getSelectBases",
            type: "GET",
            dataType: 'json',
            timeout: 20 * 1000,
            contentType: 'application/json;charset=UTF-8',
            success: function (json) {
                //console.log(json);
                if (json != null && json.code == 100) {                 	
                 	if(json.data.length > 0){
	                	for (var i in json.data) {
	                        $("#word_base").append("<option value=\"" + json.data[i].id + "\">" + json.data[i].basename + "</option>");
	                    }
                	}
                	else{
                		$("#word_base").replaceWith("<div class='alert alert-warning' role='alert'>请先添加词库！</div>");
                		$("#btnSubmit").attr('disabled',"true");
                	}
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("网络请求错误");
                $("#btnSubmit").attr('disabled',"true");
            }
        });
    }
    
    // 提交表单
    $(function () {   	
    	// 获取下拉列表
    	fillData();
    	
    	// 提交表单
    	$("#btnSubmit").click(function () {
    		
    		var English = document.getElementById("English").value;
    		var Chinese = document.getElementById("Chinese").value;
    		var BaseId = document.getElementById("word_base").value
    		
    		if (English == '') {
                layer.msg('英文单词不能为空', { icon: 0, shift: 6, time: 1000 });
                return;
            }
    		if (Chinese == '') {
                layer.msg('汉语释义不能为空', { icon: 0, shift: 6, time: 1000 });
                return;
            }
    		
            $.ajax({
                type: 'POST',
                url: 'api/word/add',
                data: { english:English,chinese:Chinese,base_id:BaseId },
                dataType: 'json',
                success: function (result) {
                    if (result != null && result.code > 100) {
                        layer.msg(result.msg, { icon: 0, shift: 6, time: 1000 })
                    } else {
                        layer.msg(result.msg, { icon: 6, shift: 4, time: 1000 });                       
                        setTimeout(function () {
                            window.location.href="Word/Index";
                        }, 1000);
                    }
                }
            });
    		
    	});
    });  
	</script>    
  </body>
</html>
