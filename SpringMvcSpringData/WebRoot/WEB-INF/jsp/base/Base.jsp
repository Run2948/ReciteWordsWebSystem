<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>词库管理</title>
<jsp:include page="../common/partial/css.jsp"></jsp:include>
</head>
<body>
	<div class="container col-md-12" id="baseDiv">
		<jsp:include page="../common/partial/top.jsp"></jsp:include>
		<div class="row clearfix">
			<div class="col-md-12 column">
				<div class="row clearfix">
					<div class="col-md-2 column">
						<div class="list-group">
							<a href="javascript:;" class="list-group-item active"> <span class="glyphicon glyphicon-list"></span> 词库列表</a> 
							<a href="javascript:;" role="button" data-toggle="modal" data-target="#addModal" class="list-group-item"> <span class="glyphicon glyphicon-plus"></span> 新建词库</a>
						</div>
					</div>
					<div class="col-md-10 column">
						<div>
							<ol class="breadcrumb">
								<li><a href="Home/Index">首页</a></li>
								<li><a href="Base/Index">词库管理</a></li>
								<li class="active">词库列表</li>
							</ol>
							<a href="" class="btn btn-default" role="button" data-toggle="modal" data-target="#addModal"><span class="glyphicon glyphicon-plus"></span> 新建词库</a> 
							<span style="padding-left: 50px;">搜索：</span> 
							<span> 
								<input type="text" placeholder="请输入词库名称搜索" v-model="searchName" class="form-control" style="width:250px; line-height:17px;display:inline-block" />
								<button class="btn btn-default" v-on:click="search">查询</button>
							</span>
						</div>
						<table id="table" class="table table-hover table-striped">
							<thead>
								<tr>
									<th>序号</th>
									<th>词库名称</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(base,baseIndex) in bases">
									<th scope="row"><input type="checkbox" name="id[]" v-bind:value="base.id"/>&nbsp;{{ baseIndex+1 }}</th>
									<td v-text="base.basename" class="label label-success" style="display: inline-block;"></td>
									<td>
										<div role="presentation" class="dropdown">
											<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">操作 <span class="caret"></span> </button>
											<ul class="dropdown-menu">
												<li><a href="javascript:void(0)" v-on:click="editInfo(baseIndex+1)">编辑</a></li>
												<li><a href="javascript:void(0)" v-on:click="del(base.id)">删除</a></li>
											</ul>
										</div>
									</td>
								</tr>
					            <tr>
				                    <th scope="row" colspan="3" v-show="this.count > 1">
				                        <input type="checkbox" id="checkall" value="" />&nbsp;&nbsp;<label for="checkall">全选</label>&nbsp;&nbsp;&nbsp;&nbsp;
				                        <a href="javascript:void(0)" class="btn btn-default" style="padding:5px 15px;" onclick="DelSelect()">批量删除</a>
				                    </th>
					            </tr>								
							</tbody>
						</table>
						<!-- 由于数据太多，所以必须以分页的形式存在 -->
						<nav aria-label="Page navigation" v-if="totalPage > 1">
							<ul class="pagination">
								<li v-bind:class="{ disabled: currentPage == 1}" v-on:click="changePage(currentPage - 1)"><a href="javascript:void(0)" aria-label="Previous"> <span aria-hidden="true">上一页</span></a></li>
								<li v-if="totalPage > 10 && currentPage >= 6" v-on:click="changePage(1)"><a href="javascript:void(0)">1</a></li>
								<li v-if="totalPage > 10 && currentPage >= 6" class="disabled"> <a href="javascript:void(0)">...</a></li>
								<li v-if="(totalPage <= 10 ) || (currentPage <= 5 && p <= 8) || (currentPage >= totalPage - 5 && p > totalPage - 8) || (currentPage > 5 && currentPage <= totalPage - 5 && p > currentPage - 3 && p < currentPage + 4)" v-for="p in totalPage" v-bind:class="{active: p == currentPage}" v-on:click="changePage(p)"><a href="javascript:void(0)" v-text="p"></a></li>
								<li v-if="totalPage > 10 && currentPage < totalPage - 5" class="disabled"><a href="javascript:void(0)">...</a></li>
								<li v-if="totalPage > 10 && currentPage < totalPage - 5" v-on:click="changePage(totalPage)"><a href="javascript:void(0)" v-text="totalPage"></a></li>
								<li v-bind:class="{disabled: currentPage == totalPage}" v-on:click="changePage(currentPage + 1)"><a href="javascript:void(0)" aria-label="Next"> <span aria-hidden="true">下一页</span></a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- add  Modal -->
	<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">添加词库</h4>
				</div>
				<div class="modal-body">
					<form action="#">
						<div class="form-group">
							<label for="addbasename">词库名称</label> <input type="text" id="addbasename" class="form-control" placeholder="请输入词库名称"/>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" onclick="add()">提交</button>
				</div>
			</div>
		</div>
	</div>
	<!-- edit Modal -->
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">编辑词库</h4>
				</div>
				<div class="modal-body">
					<form action="#">
						<input type="hidden" id="editbaseid" class="form-control" value=""/>
						<div class="form-group">
							<label for="editbasename">词库名称</label> <input type="text" id="editbasename" class="form-control" placeholder="请输入词库名称" value=""/>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" onclick="edit()">提交</button>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../common/partial/js.jsp"></jsp:include>
	<script type="text/javascript">
		//  请求数据的主体方法
		var baseApp = new Vue(
			{
				el : '#baseDiv',
				data : {
					bases : [],
					count : null,
					currentPage : 1,
					totalPage : null,
					searchName : ""
				},
				mounted : function() {

					// 穿插一个 全选方法
					$("#checkall").click(function() {
						if (this.checked) {
							$("input[name='id[]']").each(function() {
								this.checked = true;
							})
						} else {
							$("input[name='id[]']").each(function() {
								this.checked = false;
							})
						}
					});

					// 请求数据
					var temp = this;
					$.ajax({
						url : 'api/base/getBases?pageSize=8&pageIndex=0',
						type : "GET",
						dataType : 'json',
						timeout : 20 * 1000,
						contentType : 'application/json;charset=UTF-8',
						success : function(json) {
							if (json.code == 100) {
								temp.bases = json.data.content;
								temp.currentPage = 1;
								temp.count = json.data.totalElements;
							}
						},
						error : function(XMLHttpRequest, textStatus, errorThrown) {
							alert("网络请求错误")
						}
					})
				},
				watch : {
					"count" : function(val, oldVal) {
						this.totalPage = parseInt(val / 8) + (val % 8 > 0 ? 1 : 0);
					}
				},
				methods : {
					search : function() {
						var temp = this;
						$.ajax(
								{
									url : 'api/base/getBases?pageSize=8&pageIndex=0&basename='+ temp.searchName,
									type : "GET",
									dataType : 'json',
									timeout : 20 * 1000,
									contentType : 'application/json;charset=UTF-8',
									success : function(json) {
										if (json.code == 100 && json.data.totalElements > 0) {
											temp.bases = json.data.content;
											temp.currentPage = 1;
											temp.count = json.data.totalElements;
										} else {
											alert("你搜索的内容不存在啊")
										}
									},
									error : function(XMLHttpRequest,
											textStatus, errorThrown) {
										//console.log("网络请求错误")
										alert("网络错误")
									},
								})
					},
					changePage : function(p) {
						if (p <= 0) {
							this.currentPage = 1;
						} else if (p > this.totalPage) {
							this.currentPage = this.totalPage;
						} else {
							this.currentPage = p;
						}
						this.submit(this.currentPage);
					},
					submit : function(page) {
						if (!page) {
							page = 1;
						}
						var temp = this;
						$.ajax({
							url : 'api/base/getBases?pageSize=8&pageIndex='+ (page - 1),
							type : 'GET',
							dateType : 'json',
							timeout : 20 * 1000,
							contentType : 'application/json;charset=UTF-8',
							success : function(json) {
								if (json.code == 100) {
									temp.bases = json.data.content;
									temp.currentPage = page;
								}
							}
						})
					}
				}
			})
		
        // 单个删除
        function del(id) {
            //询问框
            layer.confirm('您确定要删除该词库吗？删除该词库前请先清空该词库中的所有单词。', {
                shade: false,
                btn: ['确定', '取消'] //按钮
            }, function () {
                $.ajax({
                    type: 'POST',
                    url: 'api/base/delete',
                    data: { id: id },
                    dataType: 'json',
                    success: function (result) {
                        if (result != null && result.code > 100) {
                            layer.msg(result.msg, { icon: 0, shift: 6, time: 1000 })
                        } else {
                            layer.msg(result.msg, { icon: 6, shift: 4, time: 1000 });
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                });
            }, function () {

            });
        }

        //批量删除
        function DelSelect() {
            var becheckbox = '[';
            $("input[name='id[]']").each(function () { //遍历table里的全部checkbox
                if (this.checked == true) //如果被选中
                    becheckbox += $(this).val() + ","; //获取被选中的值
            });
            if (becheckbox.length > 1) { //如果获取到
                becheckbox = becheckbox.substring(0, becheckbox.length - 1); //把最后一个逗号去掉
                becheckbox += ']';
                //询问框
                layer.confirm('您确定要删除所有选中词库吗?删除词库前请先清空词库中的所有单词。', {
                    shade: false,
                    btn: ['确定', '取消'] //按钮
                }, function () {
                    $.ajax({
                        type: 'POST',
                        url: 'api/base/deleteAll',
                        data: { ids: becheckbox },
                        dataType: 'json',
                        success: function (result) {
                            if (result != null && result.code > 100) {
                                layer.msg(result.msg, { icon: 0, shift: 6, time: 1000 })
                            } else {
                                layer.msg(result.msg, { icon: 6, shift: 4, time: 1000 });
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                            }
                        }
                    });
                }, function () {

                });
            } else {
                layer.msg("请至少选中一项！", { icon: 0, shift: 6, time: 1000 })
            }
        }
    
        // 模态添加
        function add() {
            var BaseName = document.getElementById("addbasename").value;

            if(BaseName =='')
            {
                layer.msg('词库名称不能为空！');
                return;
            }

            $.ajax({
                type: 'POST',
                url: 'api/base/add',
                data: { basename:BaseName },
                dataType: 'json',
                success: function (result) {
                    $('#addModal').modal('hide');
                    if (result != null && result.code > 100) {
                        layer.msg(result.msg, { icon: 0, shift: 6, time: 1000 })
                    } else {
                        layer.msg(result.msg, { icon: 6, shift: 4, time: 1000 });                       
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                }
            });
        }

        // 模态修改
        function editInfo(id) {
            var Id = document.getElementById("table").rows[id].cells[0].firstChild.value;
            var BaseName = document.getElementById("table").rows[id].cells[1].innerText;
            
            $("#editbaseid").val(Id.trim());
            $("#editbasename").val(BaseName.trim());

            $('#editModal').modal('show');
        }

        // 修改后提交
        function edit() {
            var Id = document.getElementById("editbaseid").value;
            var BaseName = document.getElementById("editbasename").value;

            if(BaseName =='')
            {
                layer.msg('词库名称不能为空！');
                return;
            }
            
            $.ajax({
                type: 'POST',
                url: 'api/base/edit',
                data: { basename:BaseName, id: Id },
                dataType: 'json',
                success: function (result) {
                    $('#editModal').modal('hide');
                    if (result != null && result.code > 100) {
                        layer.msg(result.msg, { icon: 0, shift: 6, time: 1000 })
                    } else {
                        layer.msg(result.msg, { icon: 6, shift: 4, time: 1000 });
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                }
            });
        }

	</script>
</body>
</html>
