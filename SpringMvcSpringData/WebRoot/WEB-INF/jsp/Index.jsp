<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>首页</title>
<jsp:include page="common/partial/css.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
	<div class="container col-md-12">
		<jsp:include page="common/partial/top.jsp"></jsp:include>
		<div class="row clearfix">
			<div class="col-md-12 column">
				<div class="alert alert-info alert-dismissible fade in" role="alert"><h4>进行单词测试前，必须手动选择词库！</h4><p>正常用户在使用本系统进行单词测试前，可以点击【立即选择】选择适合自己词库进行。体验用户可以点击【首次体验】选择默认词库中的单词进行测试体验。</p><p class="center-block"><button type="button" class="btn btn-success" role="button" data-toggle="modal" data-target="#selectModal">立即选择</button>&nbsp;&nbsp;<button type="button" class="btn btn-default" onclick="select(1)">首次体验</button></p></div>
				<div id='quiz-container'></div>
			</div>
		</div>
		<!-- select Modal -->
		<div class="modal fade" id="selectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">单词测试</h4>
					</div>
					<div class="modal-body">
						<form action="#">						
				            <div class="form-group">
				                <label for="word_base">选择词库</label>
				                <select class="form-control" style="width:200px;" id="word_base" name="wordBase">
				                    <!-- <option value=""></option> -->
				                </select>
				            </div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
						<button type="button" class="btn btn-primary" onclick="select(0);">确认</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="common/partial/js.jsp"></jsp:include>
	<script type="text/javascript" src="js/quiz.js"></script>
	<script type="text/javascript">
		var init = {
			'questions' : []
		};

		$(function() {					
	    	// 获取下拉列表
	    	fillData();	
		});
		
		// 开始测试
		function start(data){
			if(data != null){
				$('#quiz-container').jquizzy({
					questions : data.questions
				});
			}
		}
		
		// 选择词库
		function select(id){
			if(id === 0){
				id = document.getElementById("word_base").value;
			}
			 $.ajax({
	            type: "POST",
	            url: "api/quiz",
	            data:{ base_id:id },
	            dataType: 'json',
	            success: function (result) {
	            	$('#selectModal').modal('hide');
	                console.log(result);
	                if (result != null && result.code > 100) {
                        layer.msg(result.msg, { icon: 0, shift: 6, time: 1000 })
                    } else {
                    	init.questions = result.questions;
                    	start(init);
                    }
	            },
	            error: function (XMLHttpRequest, textStatus, errorThrown) {
	                alert("网络请求错误");
	            }
	        });
		}
		
		// 获取下拉列表
	    function fillData() {
	        $.ajax({
	            url: "api/base/getSelectBases",
	            type: "GET",
	            dataType: 'json',
	            timeout: 20 * 1000,
	            contentType: 'application/json;charset=UTF-8',
	            success: function (json) {
	                //console.log(json);
	                if (json != null && json.code == 100) {                 	
	                 	if(json.data.length > 0){
		                	for (var i in json.data) {
		                        $("#word_base").append("<option value=\"" + json.data[i].id + "\">" + json.data[i].basename + "</option>");
		                    }
	                	}
	                	else{
	                		$("#word_base").replaceWith("<div class='alert alert-warning' role='alert'>请先添加词库！</div>");
	                		$("#btnSubmit").attr('disabled',"true");
	                	}
	                }
	            },
	            error: function (XMLHttpRequest, textStatus, errorThrown) {
	                alert("网络请求错误");
	                $("#btnSubmit").attr('disabled',"true");
	            }
	        });
	    }
	</script>
</body>
</html>
