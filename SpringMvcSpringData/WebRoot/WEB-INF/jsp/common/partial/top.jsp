<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="Home/Index">我爱记单词</a>
        </div>
        <div>
            <ul class="nav navbar-nav" id="menu-bar">
                <li><a href="Home/Index">记单词</a></li>
                <li><a href="Base/Index">词库管理</a></li>
                <li><a href="Word/Index">单词管理</a></li>
                <li><a href="User/Index">用户管理</a></li>
                <li><a href="Help/Index">帮助关于</a></li>
            </ul>
        </div>
        <div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:void(0)" role="button" data-toggle="modal" data-target="#editpasswordModal"><span class="glyphicon glyphicon-user"></span> ${userToken.username}</a></li>
                <li><a href="Account/Logout"><span class="glyphicon glyphicon-log-in"></span> 注销</a></li>
            </ul>
        </div>
    </div>
</nav>
	<!-- edit Password Modal -->
	<div class="modal fade" id="editpasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">修改密码</h4>
				</div>
				<div class="modal-body">
					<form action="#">
						<input type="hidden" id="updateuserid" class="form-control" value="${userToken.id }"/>
						<div class="form-group">
							<label for="updateusername">用户名称</label> <input type="text" readonly id="updateusername" class="form-control" placeholder="请输入用户名称" value="${userToken.username }"/>
						</div>
						<div class="form-group">
							<label for="updatepassword">原密码</label> <input type="password" id="updatepassword" class="form-control" placeholder="请输入原密码" value=""/>
						</div>
						<div class="form-group">
							<label for="newpassword">新密码</label> <input type="password" id="newpassword" class="form-control" placeholder="请输入新密码" value=""/>
						</div>
						<div class="form-group">
							<label for="updateconfrim">确认密码</label> <input type="password" id="updateconfrim" class="form-control" placeholder="请输入确认密码" value=""/>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" onclick="password()">提交</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function password(){
			
			var UserName = $("#updateusername").val();
			var PassWord = $("#updatepassword").val();
			var NewPass = $("#newpassword").val();
			var Confrim = $("#updateconfrim").val();
			
            if(UserName =='')
            {
                layer.msg('用户名称不能为空！', { icon: 0, shift: 6, time: 1000 });
                return;
            }
            if(PassWord =='')
            {
                layer.msg('原密码不能为空！', { icon: 0, shift: 6, time: 1000 });
                return;
            }
            if(NewPass =='')
            {
                layer.msg('新密码不能为空！', { icon: 0, shift: 6, time: 1000 });
                return;
            }
            if(Confrim =='')
            {
                layer.msg('确认密码不能为空！', { icon: 0, shift: 6, time: 1000 });
                return;
            }           
            if(Confrim != NewPass)
            {
                layer.msg('两次密码不一致！', { icon: 0, shift: 6, time: 1000 });
                return;
            }
			
            $.ajax({
                type: 'POST',
                url: 'api/user/set',
                data: { id:${userToken.id },username:UserName,password:PassWord,newpass:NewPass,confrim:Confrim },
                dataType: 'json',
                success: function (result) {
                    $('#editpasswordModal').modal('hide');
                    if (result != null && result.code > 100) {
                        layer.msg(result.msg, { icon: 0, shift: 6, time: 1000 });
                    } else {
                        layer.msg(result.msg, { icon: 6, shift: 4, time: 2000 });                       
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                }
            });
		}
	</script>