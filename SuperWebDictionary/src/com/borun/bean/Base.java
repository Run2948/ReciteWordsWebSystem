package com.borun.bean;

public class Base {

	private int id;
	private String basename;
	
	
	public Base() {
		super();
	}
	
	public Base(String basename) {
		super();
		this.basename = basename;
	}
	
	
	public Base(int id, String basename) {
		super();
		this.id = id;
		this.basename = basename;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBasename() {
		return basename;
	}
	public void setBasename(String basename) {
		this.basename = basename;
	}
}
