package com.borun.bean;

public class Word {
	
	private int id;
	private String english;
	private String chinese;	
	private int base_id;
	
	public Word() {
		super();
	}
	
	public Word(String english, String chinese, int base_id) {
		super();
		this.english = english;
		this.chinese = chinese;
		this.base_id = base_id;
	}
	
	public Word(int id,String english, String chinese) {
		super();
		this.id = id;
		this.english = english;
		this.chinese = chinese;
	}
	
	public Word(int id, String english, String chinese, int base_id) {
		super();
		this.id = id;
		this.english = english;
		this.chinese = chinese;
		this.base_id = base_id;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEnglish() {
		return english;
	}
	public void setEnglish(String english) {
		this.english = english;
	}
	public String getChinese() {
		return chinese;
	}
	public void setChinese(String chinese) {
		this.chinese = chinese;
	}

	public int getBase_id() {
		return base_id;
	}

	public void setBase_id(int base_id) {
		this.base_id = base_id;
	}
	
}
