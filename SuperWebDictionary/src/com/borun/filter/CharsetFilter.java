package com.borun.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class CharsetFilter implements Filter{

	private String CharSet;
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
	
		request.setCharacterEncoding(CharSet); // 统一请求编码
		response.setContentType("text/html;charset="+CharSet); // 统一响应编码
		filterChain.doFilter(request,response); // 过滤连	
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		if(filterConfig.getInitParameter("charset") != null){
			CharSet = filterConfig.getInitParameter("charset");
		}else{
			CharSet = "utf-8";
		}
	}

}
