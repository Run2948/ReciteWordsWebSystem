package com.borun.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.borun.bean.User;
import com.borun.bean.page.Page;
import com.borun.service.UserService;

public class UserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private UserService userService = new UserService();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String option = request.getParameter("option");
		Integer pageIndex = Integer.valueOf(request.getParameter("pageIndex") == null?"0":request.getParameter("pageIndex"));
		Integer pageSize = Integer.valueOf(request.getParameter("pageSize") == null?"10":request.getParameter("pageSize"));
		if(null == option){
			try {
				List<User> userlist = userService.getPagedList(new Page(pageIndex,pageSize));
				request.setAttribute("userlist", userlist);
				request.getRequestDispatcher("/list.jsp").forward(request, response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		if("".equals(option)){
			
		}
			
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
	}

}
