package com.borun.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.borun.bean.User;
import com.borun.service.UserService;

public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private UserService userService = new UserService();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html:charset=utf-8");
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		if("".equals(username) || "".equals(password)){
			return;
		}
		
		try {
			User loginUser = new User(username,password);
			if(userService.Login(loginUser)){
				request.getSession().setAttribute("loginUser", loginUser);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
