package com.borun.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.borun.bean.Base;
import com.borun.bean.Word;
import com.borun.bean.page.Page;
import com.borun.service.BaseService;
import com.borun.service.WordService;
import com.borun.util.DBUtil;

public class Test {

	public static void main(String[] args) {

		//TestConnection();
		
		//TestAddBase(new Base("词库一"));
				
		//TestBaseList();		
						
		//TestAddWord(new Word("test","测试",1));
		
		//TestUpdateWord(new Word(3,null,"测试",0));
		
		//TestFindWordBy(new Word(null,null,1));
		
		//TestPagedWordList(new Page(0,2));
		//TestPagedWordList(new Page(0,2));
		
		
		TestPagedWordListBy(new Page(0,2),new Word(0,null,"人",0));
		
	}

	
	
	private static void TestConnection(){
		try{
			Connection conn = DBUtil.getConnection();
			System.out.println(DBUtil.DATABASE_TYPE+"数据库已连接，链接状态："+(conn.isClosed()?"关闭":"打开"));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.print(DBUtil.DATABASE_TYPE+"数据库连接失败");
		}
	}
	
	
	private static void TestAddBase(Base base){
		try {
			new BaseService().add(base);
			System.out.println("添加成功！");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static void TestBaseList(){
		try {
			List<Base> list = new BaseService().getListBy(new Base("词库"));
			System.out.println("词库数量："+list.size());
			if(list.size() > 0)
				for(Base b : list){
					System.out.println("词库编号："+b.getId() + ";词库名称："+b.getBasename());
				}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
	private static void TestAddWord(Word word){
		try {
			new WordService().add(word);
			System.out.println("添加成功！");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	private static void TestUpdateWord(Word word){
		try {
			new WordService().update(word);
			System.out.println("修改成功！");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	private static void TestFindWordBy(Word word){
		try {
			List<Word> list = new WordService().getListBy(word);
			print(list);
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
	
	
	private static void TestPagedWordList(Page page){
		try {
			List<Word> list = new WordService().getPagedList(page);
			print(list);
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
	
	private static void TestPagedWordListBy(Page page,Word word){
		try {
			List<Word> list = new WordService().getPagedListBy(page,word);
			print(list);
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
	

	private static void print(List<Word> list){
		System.out.println("单词数量："+list.size());
		if(list.size() > 0)
			for(Word w : list){
				String base_name = null;
				try {
					base_name = new BaseService().findById(w.getBase_id()).getBasename();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				System.out.println("单词编号："+w.getId() + ";英文："+w.getEnglish() + ";中文："+w.getChinese()+";所属词库："+ base_name);
			}
	}
	
}
