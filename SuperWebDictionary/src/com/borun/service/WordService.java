package com.borun.service;

import java.sql.SQLException;
import java.util.List;

import com.borun.bean.Word;
import com.borun.bean.page.Page;
import com.borun.dao.WordDao;
import com.borun.service.base.Service;

public class WordService implements Service<Word>{

	private WordDao dao = null;
		
	public WordService(){
		setDao(new WordDao());
	}
	
	
	@Override
	public boolean add(Word model) throws SQLException {
		return dao.add(model);
	}

	@Override
	public boolean delete(Integer id) throws SQLException {
		return dao.delete(id);
	}

	@Override
	public boolean update(Word model) throws SQLException {
		return dao.update(model);
	}

	@Override
	public Word findById(Integer id) throws SQLException {
		return dao.findById(id);
	}

	@Override
	public Word findBy(Word model) throws SQLException {
		return dao.findBy(model);
	}

	@Override
	public List<Word> getList() throws SQLException {
		return dao.getList();
	}

	@Override
	public List<Word> getListBy(Word model) throws SQLException {
		return dao.getListBy(model);
	}

	@Override
	public List<Word> getPagedList(Page page) throws SQLException {
		return dao.getPagedList(page);
	}

	@Override
	public List<Word> getPagedListBy(Page page, Word model) throws SQLException {
		return dao.getPagedListBy(page,model);
	}


	public WordDao getDao() {
		return dao;
	}


	public void setDao(WordDao wordDao) {
		this.dao = wordDao;
	}
}
