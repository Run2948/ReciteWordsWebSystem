package com.borun.service;

import java.sql.SQLException;
import java.util.List;

import com.borun.bean.Base;
import com.borun.bean.page.Page;
import com.borun.dao.BaseDao;
import com.borun.service.base.Service;

public class BaseService implements Service<Base>{

	private BaseDao dao = null;
	
	public BaseService(){
		setDao(new BaseDao());
	}
	
	@Override
	public boolean add(Base model) throws SQLException {
		return dao.add(model);
	}

	@Override
	public boolean delete(Integer id) throws SQLException {
		return dao.delete(id);
	}

	@Override
	public boolean update(Base model) throws SQLException {
		return dao.update(model);
	}

	@Override
	public Base findById(Integer id) throws SQLException {
		return dao.findById(id);
	}

	@Override
	public Base findBy(Base model) throws SQLException {
		return dao.findBy(model);
	}

	@Override
	public List<Base> getList() throws SQLException {
		return dao.getList();
	}

	@Override
	public List<Base> getListBy(Base model) throws SQLException {
		return dao.getListBy(model);
	}

	@Override
	public List<Base> getPagedList(Page page) throws SQLException {
		return dao.getPagedList(page);
	}

	@Override
	public List<Base> getPagedListBy(Page page, Base model) throws SQLException {
		return dao.getPagedListBy(page,model);
	}

	public BaseDao getDao() {
		return dao;
	}

	public void setDao(BaseDao dao) {
		this.dao = dao;
	}

}
