package com.borun.service;

import java.sql.SQLException;
import java.util.List;

import com.borun.bean.User;
import com.borun.bean.page.Page;
import com.borun.dao.UserDao;
import com.borun.service.base.Service;

public class UserService implements Service<User>{

	private UserDao dao = null;
	
	public UserService(){
		setDao(new UserDao());
	}
	
	
	public boolean Login(User user) throws SQLException {
		return dao.findBy(user) != null;
	}

	public boolean Regist(User user) throws SQLException {
		return dao.add(user);
	}

	
	
	@Override
	public boolean add(User model) throws SQLException {
		return dao.add(model);
	}

	@Override
	public boolean delete(Integer id) throws SQLException {
		return dao.delete(id);
	}

	@Override
	public boolean update(User model) throws SQLException {
		return dao.update(model);
	}

	@Override
	public User findById(Integer id) throws SQLException {
		return dao.findById(id);
	}

	@Override
	public User findBy(User model) throws SQLException {
		return dao.findBy(model);
	}

	@Override
	public List<User> getList() throws SQLException {
		return dao.getList();
	}

	@Override
	public List<User> getListBy(User model) throws SQLException {
		return dao.getListBy(model);
	}

	@Override
	public List<User> getPagedList(Page page) throws SQLException {
		return dao.getPagedList(page);
	}

	@Override
	public List<User> getPagedListBy(Page page, User model) throws SQLException {
		return dao.getPagedListBy(page,model);
	}

	public UserDao getDao() {
		return dao;
	}

	public void setDao(UserDao dao) {
		this.dao = dao;
	}

}
