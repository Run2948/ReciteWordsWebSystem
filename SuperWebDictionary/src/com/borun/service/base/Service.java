package com.borun.service.base;

import java.sql.SQLException;
import java.util.List;

import com.borun.bean.page.Page;

public interface Service<T> {
	
	public boolean add(T model) throws SQLException;
	
	public boolean delete(Integer id) throws SQLException;
	
	public boolean update(T model) throws SQLException;
	
	public T findById(Integer id) throws SQLException;
	
	public T findBy(T model) throws SQLException;
		
	public List<T> getList() throws SQLException;
	
	public List<T> getListBy(T model) throws SQLException;
	
	public List<T> getPagedList(Page page) throws SQLException;
		
	public List<T> getPagedListBy(Page page,T model) throws SQLException;

}
