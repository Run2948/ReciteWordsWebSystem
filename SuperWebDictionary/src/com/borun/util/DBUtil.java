package com.borun.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {

	// 数据库类型：
	// mysql
	// sqlserver
	// oracle
	public final static String DATABASE_TYPE = "mysql";// "sqlserver";

	// 数据库 登录用户和密码：
	private static final String MYSQL_USER = "root";
	private static final String MYSQL_PASSWORD = "Thanks2015";

	private static final String MSSQL_USER = "sa";
	private static final String MSSQL_PASSWORD = "Thanks2015";

	private static final String ORACLE_USER = "orcl";
	private static final String ORACLE_PASSWORD = "Thanks2015";

	// 数据库名称：
	private final static String DATABASE_NAME = "WordBase";

	// 数据库驱动：
	// com.mysql.jdbc.Driver
	// com.microsoft.sqlserver.jdbc.SQLServerDriver
	// oracle.jdbc.driver.OracleDriver
	private final static String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	private final static String MSSQL_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	private final static String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";

	// 数据库服务器和端口：
	// localhost:3306
	// localhost:1433
	// 127.0.0.1:1521
	private final static String MYSQL_SERVER = "192.168.219.55";
	private final static String MSSQL_SERVER = "192.168.219.55";
	private final static String ORACLE_SERVER = "192.168.219.55";

	private final static String MYSQL_PORT = "3306";
	private final static String MSSQL_PORT = "1433";
	private final static String ORACLE_PORT = "1521";

	private final static String MYSQL_SERVER_AND_PORT = MYSQL_SERVER + ":"
			+ MYSQL_PORT;
	private final static String MSSQL_SERVER_AND_PORT = MSSQL_SERVER + ":"
			+ MSSQL_PORT;
	private final static String ORACLE_SERVER_AND_PORT = ORACLE_SERVER + ":"
			+ ORACLE_PORT;

	// 数据库连接字符串：
	// "jdbc:mysql://localhost:3306/WordBase"
	// "sqlserver://localhost:1433;DatabaseName=WordBase"
	// "jdbc:oracle:thin:@127.0.0.1:1521:WordBase"
	private static final String MSSQL_URL = "jdbc:" + DATABASE_TYPE + "://"
			+ MSSQL_SERVER_AND_PORT + ";DatabaseName=" + DATABASE_NAME;
	private static final String MYSQL_URL = "jdbc:" + DATABASE_TYPE + "://"
			+ MYSQL_SERVER_AND_PORT + "/" + DATABASE_NAME;
	private static final String ORACLE_URL = "jdbc:+ DATABASE_TYPE +:thin:@"
			+ ORACLE_SERVER_AND_PORT + ":" + DATABASE_NAME;

	private static Connection conn = null;
	// 静态代码块（将加载驱动、连接数据库放入静态块中）
	static {
		try {
			if ("sqlserver".equals(DATABASE_TYPE)) {
				// 1.加载驱动程序
				Class.forName(MSSQL_DRIVER);
				// 2.获得数据库的连接
				conn = DriverManager.getConnection(MSSQL_URL, MSSQL_USER,
						MSSQL_PASSWORD);
			} else if ("mysql".equals(DATABASE_TYPE)) {
				// 1.加载驱动程序
				Class.forName(MYSQL_DRIVER);
				// 2.获得数据库的连接
				conn = DriverManager.getConnection(MYSQL_URL, MYSQL_USER,
						MYSQL_PASSWORD);
			} else if ("oracle".equals(DATABASE_TYPE)) {
				// 1.加载驱动程序
				Class.forName(ORACLE_DRIVER);
				// 2.获得数据库的连接
				conn = DriverManager.getConnection(ORACLE_URL, ORACLE_USER,
						ORACLE_PASSWORD);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// 对外提供一个方法来获取数据库连接
	public static Connection getConnection() {
		return conn;
	}

	public static void colseConnection() {
		try {
			if (conn != null && !conn.isClosed())
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}