package com.borun.util;

import java.util.Map;

public class MapUtil {

	public static int getKey(Map<Integer, String> map, String value) {
		int key = -1;
		// Map,HashMap并没有实现Iteratable接口.不能用于增强for循环.
		for (Integer getKey : map.keySet()) {
			if (map.get(getKey).equals(value)) {
				key = getKey;
			}
		}
		return key;// 这个key肯定是最后一个满足该条件的key.
	}
}
