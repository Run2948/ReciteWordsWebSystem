<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">  
    <title>首页</title>
    <jsp:include page="partial/css.jsp"></jsp:include>
  </head>  
  <body>
     <div class="container col-md-12">
		<jsp:include page="partial/top.jsp"></jsp:include>
        <div class="row clearfix">
            <div class="col-md-12 column">   
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">帮助提示</h3>
                    </div>
                    <div class="panel-body">
                       	 亲，记得要至少保留一个“用户账号”。你懂的！
                    </div>
                </div>          
            </div>
        </div>
    </div>
    <jsp:include page="partial/js.jsp"></jsp:include>
  </body>
</html>
