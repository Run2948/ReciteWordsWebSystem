<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">  
    <title>词库管理</title>
    <jsp:include page="partial/css.jsp"></jsp:include>
  </head>  
  <body>
     <div class="container col-md-12">
		<jsp:include page="partial/top.jsp"></jsp:include>
        <div class="row clearfix">
            <div class="col-md-12 column">   
				<div class="row clearfix">
                    <div class="col-md-3 column">
                        <div class="list-group">
                            <a href="javascript:;" class="list-group-item active">单词管理</a>

                            <a href="javascript:;" class="list-group-item">
                                <span class="glyphicon glyphicon-list"></span> 查看单词
                            </a>
                            <a href="javascript:;" class="list-group-item">
                                <span class="glyphicon glyphicon-plus"></span> 新建单词
                            </a>

                        </div>
                    </div>
                    <div class="col-md-9 column">
                        <ol class="breadcrumb">
                            <li><a href="Index.jsp">首页</a></li>
                            <li><a href="Word.jsp">单词管理</a></li>
                            <li class="active">查看单词</li>
                        </ol>
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>序号</th>
                                    <th>邮箱地址</th>
                                    <th>真实姓名</th>
                                    <th>创建时间</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>@(i + 1)</td>
                                    <td>@Model.Result[i].UserName </td>
                                    <td>@Model.Result[i].Nick </td>
                                    <td>@Model.Result[i].CreateTime </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default">更多操作</button>
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="/UserManage/delete?username=@Model.Result[i].UserName">删除</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <ul class="pagination pagination-lg">
                            <li><a href="javascript:;">&laquo;</a></li>
                            <li><a href="javascript:;">1</a></li>
                            <li><a href="javascript:;">2</a></li>
                            <li><a href="javascript:;">3</a></li>
                            <li><a href="javascript:;">4</a></li>
                            <li><a href="javascript:;">5</a></li>
                            <li><a href="javascript:;">&raquo;</a></li>
                        </ul>
                    </div>
                </div>         
            </div>
        </div>
    </div>
    <jsp:include page="partial/js.jsp"></jsp:include>
  </body>
</html>
