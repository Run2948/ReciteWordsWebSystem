<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">  
    <title>登录</title>
	<jsp:include page="partial/css.jsp"></jsp:include>
	<style type="text/css">
        body{
            background:#eee;
            padding-top:80px;
        }
    </style>
  </head>
  <body>
<div class="container">
        <div class="row clearfix">
            <div class="col-md-4 column">
            </div>
            <div class="col-md-4 column">
                <h1>欢迎登陆</h1>
                <form class="form-horizontal" role="form" action="Index.jsp">
                    <div class="form-group">
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputEmail3" aria-describedby="emailHelp" placeholder="请输入您的登录账号" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="inputPassword3" placeholder="请输入您的登录密码" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input">
                               	 记住我
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary btn-block">登录</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4 column">
            </div>
        </div>
    </div>
    <jsp:include page="partial/js.jsp"></jsp:include>
  </body>
</html>
