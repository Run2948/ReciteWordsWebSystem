<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">  
    <title>注册</title>
	<jsp:include page="partial/css.jsp"></jsp:include>
	<style type="text/css">
        body{
            background:#eee;
            padding-top:80px;
        }
    </style>
  </head>
  <body>
	<div class="container">
        <div class="row clearfix">
            <div class="col-md-4 column">
            </div>
            <div class="col-md-4 column">
                <h1>欢迎注册</h1>
                <form class="form-horizontal" role="form" action="Login.jsp">
                    <div class="form-group">
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="请输入您的注册账号" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="请输入您的注册密码" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="请输入您的确认密码" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary btn-block">注册</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4 column">
            </div>
        </div>
    </div>
    <jsp:include page="partial/js.jsp"></jsp:include>
  </body>
</html>
