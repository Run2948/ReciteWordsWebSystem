<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="Index.jsp">我爱记单词</a>
        </div>
        <div>
            <ul class="nav navbar-nav" id="menu-bar">
                <li><a href="Index.jsp">记单词</a></li>
                <li><a href="Base.jsp">词库管理</a></li>
                <li><a href="Word.jsp">单词管理</a></li>
                <li><a href="Help.jsp">帮助</a></li>
            </ul>
        </div>
        <div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:;"><span class="glyphicon glyphicon-user"></span> 朱锦润</a></li>
                <li><a href="Login.jsp"><span class="glyphicon glyphicon-log-in"></span> 注销</a></li>
            </ul>
        </div>
    </div>
</nav>